# ACME Digital Banking Angular Application

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 13 and [Node.js](https://nodejs.org/en/about/releases) version 14.
It provides a baseline code base for managing a bank account.
This project has a complete development environment set up, including build, test, deploy, simple components, service, s and templates.

## Table of Contents

- [Project Structure](#project-structure)
- [Available npm scripts](#available-npm-scripts)
- [State management](#state-management)
- [Build and deployment](#build-and-deployment)
- [Further help](#further-help)

## Project Structure

```html
├── cicd
|  ├── build
|  |  ├── Jenkinsfie
├──├── deploy
|  |  ├── deploy-to-swarm.yaml
├──├── docker
|  |  ├── Dockerfile
|  |  ├── nginx.conf
├──├── lighthouse
|  |  ├── lighthouserc.json
├── src
|  ├── app
|  |  ├── core
|  |  |  ├── models
|  |  |  ├── services
|  |  |  └── core.module.ts
|  |  ├── features
|  |  |  ├── feature - (example: account)
|  |  ├── shared
|  |  ├── app.component.html
|  |  ├── app.component.scss
|  |  ├── app.component.spec.ts
|  |  ├── app.component.ts
|  |  └── app.module.ts
|  ├── assets
|  ├── environments
|  |  ├── environment.prod.ts
|  |  └── environment.ts
|  ├── favicon.ico
|  ├── index.html
|  ├── main.ts
|  ├── polyfills.ts
|  ├── styles.scss
|  └── jest.config.js
|  └── jestGlobalMocks.ts
|  └── setupJest.ts
├── .prettierrc.json
├── angular.json
├── commitlint.config.js
├── jest.config.js
├── package-lock.json
├── package.json
├── README.md
├── tsconfig.app.json
├── tsconfig.json
├── tsconfig.spec.json
```

The workspace root folder contains various support and configuration files, and a README file with generated descriptive text that you can customize.
All of the app's code goes in a folder named `src`.

In Angular, everything is organized in modules, and every application have at least one of them, the `app` root module. The `app` module is the entry point of the application, and is the module that Angular uses to bootstrap the application.
The `core` module takes on the role of the `app` root module, but is not the module that gets bootstrapped by Angular at run-time.
The common denominator between the files present here is that we only need to load them once, and that is at run-time, which makes them singleton.
The `features` folder contains all different feature modules, in this case we have account as a feature.
These modules are independent of each other.
This allows Angular to load only the module it requires to display the request thereby saving bandwidth and speeding the entire application.
Which means the module isn't loaded before the user actually accesses the route.
Do declare components, directives, and pipes in the `shared` module when those items will be re-used and referenced by the components declared in other feature modules.
The `shared` module shouldn't have any dependency to the rest of the application.

The global styles for the project are placed in a `scss` folder under `assets`.
These files can be imported by other scss files.
The `styles.scss` file imports all the partials to apply their styling.

## Available npm scripts

The scripts in [package.json](package.json) file were built with simplicity in mind to automate as much repetitive tasks as possible and focus on what really matters.
All the commands should be executed in a console inside the root directory.

## State management

In this project I have used used NgRx to manage the state of the application.
Reference:
[NgRx](https://ngrx.io/) is a state management system that is based on the [Redux](https://redux.js.org/) pattern.
NgRx is mainly for managing global state across an entire application.

## Build and deployment

Run `npm run build` to build the project. The build artifacts will be stored in the `dist` directory. Use the `--prod` flag for a production build.
In `cicd` folder you can find scripts for your [Jenkins](https://www.jenkins.io/) CI pipeline, a Dockerfile, [Nginx](https://www.nginx.com/) configuration and an example for deploying your application to [Docker Swarm](https://docs.docker.com/engine/swarm/).

## Further help & reference

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI Overview and Command Reference](https://angular.io/cli) page.