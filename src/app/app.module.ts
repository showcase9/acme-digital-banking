import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppComponent } from './app.component';
import { CoreModule } from './core/core.module';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { environment } from '../environments/environment';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { AccountsEffects } from './store/effects/account.effects';
import { ACCOUNTS_FEATURE_KEY } from './store/account.state';
import { reducer } from './store/reducers/account.reducer';
import { AccountFacade } from './store/account.facade';
import { AccountsComponent } from './features/accounts/accounts.component';
import { WithdrawDialogComponent } from './features/withdraw-dialog/withdraw-dialog.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { MatToolbarModule } from '@angular/material/toolbar';
import { MatDialogModule } from '@angular/material/dialog';
import { MatInputModule } from '@angular/material/input';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

export const MATERIAL_UI_COMPONENTS = [
  MatToolbarModule,
  MatDialogModule,
  MatInputModule
];

@NgModule({
  declarations: [
    AppComponent,
    AccountsComponent,
    WithdrawDialogComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    StoreModule.forRoot({}),
    StoreModule.forFeature(ACCOUNTS_FEATURE_KEY, reducer),
    StoreDevtoolsModule.instrument({ maxAge: 25, logOnly: environment.production }),
    EffectsModule.forRoot([
      AccountsEffects
    ]),
    CoreModule,
    ...MATERIAL_UI_COMPONENTS,
    FormsModule,
    ReactiveFormsModule,
  ],
  providers: [AccountFacade],
  bootstrap: [AppComponent]
})
export class AppModule { }
