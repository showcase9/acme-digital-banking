export interface Account {
    'account_number': string;
    'account_type': AccountType;
    'balance': string;
}


export enum AccountType {
    CHEQUE = 'cheque',
    SAVINGS = 'savings'
}

export class BankAccount {
    private static DEFAULT_MAX_OVERDRAFT = 500; // as per business rules

    accountNumber!: number;
    accountType!: AccountType;
    balance!: number;
    overdraft!: number;

    constructor(private _newAccount: Account) {
        this.setupNewAccount(this._newAccount);
    }

    /**
     * setup a valid bank account underwise create a new account (N.B: implementation might deffer depending on business cases)
     * @param newAccount -> Account
     */
    private setupNewAccount(newAccount: Account): void {
        this.accountNumber = Number(newAccount['account_number']) || this.generateAccountNumber();
        this.accountType = newAccount['account_type'] || AccountType.SAVINGS;
        this.balance = Number(newAccount['balance']) || 0;
        this.overdraft = this.assignDefaultOverdraft(newAccount['account_type'] as AccountType);
    }

    private assignDefaultOverdraft(accountType: AccountType): number {
        return accountType === AccountType.CHEQUE ? BankAccount.DEFAULT_MAX_OVERDRAFT : 0;
    }

    private generateAccountNumber(): number {
        return Math.floor((Math.random() * 10000000000) + 1); // dummy account number for testing
    }

    get availabe(): number {
        return this.balance + this.overdraft;
    }

    get canWithdraw(): boolean {
        return this.availabe > 0;
    }

    authorizeWithdrawal(amount: number): boolean {
        return amount <= this.availabe;
    }
}
