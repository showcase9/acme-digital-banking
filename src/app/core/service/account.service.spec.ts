import { getTestBed, TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { AccountService } from './account.service';
import { first } from 'rxjs';
import { AccountType, BankAccount } from '../model/account.model';
import { environment } from '../../../environments/environment';

describe('AccountService', () => {
    let injector: TestBed;
    let httpMock: HttpTestingController;
    let service: AccountService;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [HttpClientTestingModule],
            providers: [AccountService],
        });
        injector = getTestBed();
        httpMock = injector.get(HttpTestingController);
        service = TestBed.get(AccountService);
    });

    afterEach(() => {
        httpMock.verify();
    });

    it('should get available accounts', () => {
        service.getAccounts()
            .pipe(
                first(),
            )
            .subscribe((data) => {
                expect(data).toBeInstanceOf(BankAccount);
                expect(data.length > 0).toBe(true);
            });

        const req = httpMock.expectOne(`${environment.baseUrl}/api/accounts`);
        expect(req.request.method).toBe('GET');
        req.flush([new BankAccount({
            account_number: '12345',
            account_type: AccountType.CHEQUE,
            balance: '500'
        })]);
    });
});