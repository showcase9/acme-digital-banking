import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map, Observable } from 'rxjs';
import { Account, BankAccount } from '../model/account.model';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AccountService {

  constructor(private readonly http: HttpClient) { }

  getAccounts(): Observable<BankAccount[]> {
    return this.http.get<Account[]>(`${environment.baseUrl}/api/accounts`)
      .pipe(
        map(accounts => accounts.map(account => new BankAccount(account)))
      );
  }
}
