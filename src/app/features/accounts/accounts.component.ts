import { Component, OnInit, OnDestroy } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { filter, map, Subscription, switchMap } from 'rxjs';
import { AccountFacade } from 'src/app/store/account.facade';
import { AccountType, BankAccount } from '../../core/model/account.model';
import { WithdrawDialogComponent } from '../withdraw-dialog/withdraw-dialog.component';

@Component({
  selector: 'app-accounts',
  templateUrl: './accounts.component.html'
})
export class AccountsComponent implements OnInit, OnDestroy {
  accounts: BankAccount[] = [];
  withdrawalAmount = 0;

  private subscriptions: Subscription[] = [];

  constructor(
    private readonly accountFacade: AccountFacade,
    private readonly dialog: MatDialog
  ) { }

  ngOnInit(): void {
    this.accountFacade.loadAccounts();
    this.subscriptions.push(
      this.accountFacade.loaded$
        .pipe(
          filter((isLoaded: boolean) => isLoaded === true),
          switchMap(() => this.accountFacade.getAccounts$)
        )
        .subscribe((accounts: BankAccount[]) => this.accounts = accounts)
    );
  }

  ngOnDestroy() {
    this.subscriptions.forEach(subscription => subscription.unsubscribe())
  }

  get balance(): number {
    return this.accounts.reduce((accumulator, current) => accumulator + current.balance, 0);
  }

  withdrawCash(account: BankAccount) {
    const dialogRef = this.dialog.open(WithdrawDialogComponent, {
      width: 'auto',
      height: '250px',
      data: {
        account: account,
        amount: this.withdrawalAmount
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.withdrawalAmount = Number(result);
        this.withdraw(account, this.withdrawalAmount);
      }
    });
  }

  private async withdraw(withdrawalAccount: BankAccount, withdrawalAmount: number) {
    let account = withdrawalAccount;
    if (!account.canWithdraw) {
      alert('Insuffient funds!');
    } else {
      const withdrawalAuthorized: boolean = this.handleWithdrawal(account, withdrawalAmount);

      if (withdrawalAuthorized) {
        const newBalance = withdrawalAccount.balance - withdrawalAmount;
        this.accountFacade.updateAccount(withdrawalAccount.accountNumber, newBalance);
        alert('Success'); // message as per business case
      } else {
        alert('Insuffient funds!');
      }
    }
  }

  private handleWithdrawal(withDrawalbankAccount: BankAccount, withdrawalAmount: number): boolean {
    switch (withDrawalbankAccount.accountType) {
      case AccountType.CHEQUE: {
        // direct withdrawal because can have an overdraft
        return withDrawalbankAccount.authorizeWithdrawal(withdrawalAmount);
      }
      case AccountType.SAVINGS: {
        if (withDrawalbankAccount.balance >= withdrawalAmount) {
          return withDrawalbankAccount.authorizeWithdrawal(withdrawalAmount);
        } else {
          return false; // no need to further withdraw in this case
        }
      }
    }
  }
}
