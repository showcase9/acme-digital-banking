import { Injectable } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { AccountState } from './account.state';
import * as accountSelectors from './selectors/account.selector';
import * as accountActions from './actions/account.action';
import { BankAccount } from '../core/model/account.model';

@Injectable()
export class AccountFacade {
    readonly loaded$: Observable<boolean> = this.store.pipe(
        select(accountSelectors.getAccountsLoaded)
    );
    readonly getAccounts$: Observable<BankAccount[]> = this.store.pipe(
        select(accountSelectors.getAccounts)
    );

    constructor(private readonly store: Store<AccountState>) { }

    init(): void {
        this.store.dispatch(accountActions.Init());
    }

    loadAccounts(): void {
        this.store.dispatch(accountActions.GetAccountsAction());
    }

   updateAccount(accountNumber: number, newBalance: number): void {
        this.store.dispatch(accountActions.UpateAccountAction({ accountNumber, newBalance }));
    }
}
