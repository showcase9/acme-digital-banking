import { BankAccount } from '../core/model/account.model';

export const ACCOUNTS_FEATURE_KEY = 'accounts';

export interface AccountState {
    accounts: BankAccount[];
    loaded: boolean;
    error?: string | null;
}

export const initialState: AccountState = {
    accounts: [],
    loaded: false,
    error: null
};
