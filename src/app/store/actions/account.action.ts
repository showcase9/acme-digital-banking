import { createAction, props } from '@ngrx/store';
import { BankAccount } from 'src/app/core/model/account.model';

export enum AccountActionTypes {
    INIT = '[BankAccount] Init',
    GET_ACCOUNTS = '[BankAccount] Get Accounts',
    GET_ACCOUNTS_SUCCESS = '[BankAccount] Get Accounts Success',
    GET_ACCOUNTS_FAILURE = '[BankAccount] Get Accounts Failure',
    UPDATE_ACCOUNT = '[BankAccount] Update Accounts'
}

export const Init = createAction(AccountActionTypes.INIT);

export const GetAccountsAction = createAction(AccountActionTypes.GET_ACCOUNTS);

export const GetAccountsSuccessAction = createAction(
    AccountActionTypes.GET_ACCOUNTS_SUCCESS,
    props<{ accounts: BankAccount[] }>()
);

export const GetAccountsFailureAction = createAction(
    AccountActionTypes.GET_ACCOUNTS_FAILURE,
    props<{ error: string | null }>()
);

export const UpateAccountAction = createAction(
    AccountActionTypes.UPDATE_ACCOUNT,
    props<{ accountNumber: number, newBalance: number }>()
);

