import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { catchError, map, Observable, of, switchMap } from 'rxjs';
import { BankAccount } from '../../core/model/account.model';
import { AccountService } from '../../core/service/account.service';
import { AccountActionTypes } from '../actions/account.action';
import * as accountActions from '../actions/account.action';

@Injectable()
export class AccountsEffects {
    constructor(
        private readonly actions$: Actions,
        private readonly accountService: AccountService
    ) { }

    public readonly getAccounts$: Observable<any> = createEffect(() => {
        return this.actions$.pipe(
            ofType(AccountActionTypes.GET_ACCOUNTS),
            switchMap(() => this.accountService.getAccounts()),
            map((accounts: BankAccount[]) => accountActions.GetAccountsSuccessAction({ accounts })),
            catchError((error: string | null) =>
                of(accountActions.GetAccountsFailureAction({ error }))
            )
        );
    });
}
