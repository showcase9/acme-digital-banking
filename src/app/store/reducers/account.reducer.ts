import { Action, createReducer, on } from '@ngrx/store';
import * as accountActions from '../actions/account.action';
import { AccountState, initialState } from '../account.state';
import { BankAccount } from 'src/app/core/model/account.model';

export const accountsReducer = createReducer(
    initialState,
    on(accountActions.GetAccountsAction, (state) => ({
        ...state,
        loaded: false,
    })),
    on(accountActions.GetAccountsSuccessAction, (state, { accounts }) => ({
        ...state,
        accounts,
        loaded: true,
    })),
    on(accountActions.GetAccountsFailureAction, (state, { error }) => ({
        ...state,
        error,
    })),
    on(accountActions.UpateAccountAction, (state, data) => {
        let oldAccounts = [...state.accounts];
        const foundAccount = oldAccounts.find(account => account.accountNumber === data.accountNumber);
        const index = oldAccounts.findIndex(account => account.accountNumber === data.accountNumber);
    
        let accounts = [...oldAccounts.filter(account => account.accountNumber !== data.accountNumber)];

        if (foundAccount) {
            let account = new BankAccount({
                'account_number': `${foundAccount.accountNumber}`,
                'account_type': foundAccount.accountType,
                'balance': `${data.newBalance}`
            });
            accounts.splice(index, 0, account);
        }
        return {
            ...state,
            accounts
        }
    })
);

export function reducer(state: AccountState | undefined, action: Action) {
    return accountsReducer(state, action);
}
