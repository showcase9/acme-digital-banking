import { createFeatureSelector, createSelector } from '@ngrx/store';
import { AccountState, ACCOUNTS_FEATURE_KEY } from '../account.state';

export const getAccountsState =
  createFeatureSelector<AccountState>(ACCOUNTS_FEATURE_KEY);

export const getAccountsLoaded = createSelector(
  getAccountsState,
  (state: AccountState) => state && state.loaded
);

export const getAccountsError = createSelector(
  getAccountsState,
  (state: AccountState) => state.error
);

export const getAccounts = createSelector(
  getAccountsState,
  (state: AccountState) => state.accounts
);
